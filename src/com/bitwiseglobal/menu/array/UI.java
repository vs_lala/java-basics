package com.bitwiseglobal.menu.array;

import java.util.Scanner;

public class UI {
	
	private int ch = 0;
	private Scanner in;
	ArrayManipulation am = new ArrayManipulation ();
	
	public UI () {
		this.getUI();
		this.getUserChoice();
	}

	// this method will take user input and store it in int ch variable;
	private void getUserChoice() {
		in = new Scanner (System.in);
		System.out.println("Enter the desired choice");
		this.ch = Integer.parseInt(in.nextLine());
	}

	private void getUI() {
		do {
			this.mainPage(); 		// calls the choice method to display text
			this.getUserChoice();   // get the user input choice i.e., 1,2,3,4,0
			this.performTask();		// calls the switch statement and provide the user choice to it
		} while (ch != 0);
	}

	private void performTask() {
		Scanner in = new Scanner (System.in);
		switch (this.ch) {
			case 1:
			{
				System.out.println("Enter the size for the array");
				int size = Integer.parseInt(in.nextLine());
				am.setSize(size);
				int [] temp = new int [am.getSize()];
				am.setArrayValues(temp);
				break;
			}
			case 2:
			{
				System.out.println("Enter the size for the array");
				int size = Integer.parseInt(in.nextLine());
				am.setSize(size);
				double [] temp = new double [am.getSize()];
				am.setArrayValues(temp);
				break;
			}
			case 3:
			{	
				System.out.println("Enter the size for the array");
				int size = Integer.parseInt(in.nextLine());
				am.setSize(size);
				String [] temp = new String [am.getSize()];
				am.setArrayValues(temp);
				break;
			}
			case 4:
			{
				System.out.println("Printing Integer Array...");
				am.printArray(am.getIntegerArray());
				break;
			}
			case 5:
			{
				System.out.println("Printing Double Array...");
				am.printArray(am.getDoubleArray());
			}
				break;
			case 6:
			{
				System.out.println("Printing String Array...");
				am.printArray(am.getStringArray());
			}
				break;
			case 0:
				System.exit (0);
				break;
			default:
				
				break;
		}
		
	}

	private void mainPage() {
		System.out.println("1. Integer Array");
		System.out.println("2. Double Array");
		System.out.println("3. String Array");
		System.out.println("4. Print Integer Array");
		System.out.println("5. Print Double Array");
		System.out.println("6. Print String Array");
		System.out.println("0. Exit");
	}
}
