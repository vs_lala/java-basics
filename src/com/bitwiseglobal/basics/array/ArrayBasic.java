package com.bitwiseglobal.basics.array;

public class ArrayBasic {

	int arrayName[] = new int[5]; // declare array

	public ArrayBasic() {
		this.addIntegers();
		this.printArray();
	}

	private void printArray() {

		for (int i = 0; i < arrayName.length; i++) {
			System.out.println(arrayName[i]);
		}

	}

	private void addIntegers() {

		// define array
		arrayName[0] = 23;
		arrayName[1] = 26;
		arrayName[2] = 45;
		arrayName[3] = 12;
		arrayName[4] = 67;

	}

	public void testArray() {
			int[] number = { 2, 3, 4, 6, 8 };
			
			try {
				for (int i = 0; i < number.length; i++) {
					System.out.println(number[i] + " ");
				}

			} catch (ArrayIndexOutOfBoundsException e) {
				System.out.println("Kya biya bas 4 tak hi :D :D");
			}

	}

}
