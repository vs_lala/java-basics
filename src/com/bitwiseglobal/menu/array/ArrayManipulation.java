package com.bitwiseglobal.menu.array;

import java.util.Scanner;

public class ArrayManipulation {
	
	private int [] integerArray;
	private String [] stringArray;
	private double [] doubleArray;
	private int size;
	
	// setters/ mutators
	public void setSize (int size) { this.size = size; }

	
	// getters/accessors
	public int getSize () { return this.size; }
	public String [] getStringArray () { return this.stringArray ; }
	public int [] getIntegerArray () { return this.integerArray ; }
	public double [] getDoubleArray () { return this.doubleArray ; }
	
	// integer array setter
	public void setArrayValues (int [] array) {
		Scanner in = new Scanner (System.in);
		System.out.println("Enter the values for array");
		
		for (int i=0; i < this.size; i++) {
			int val = Integer.parseInt(in.nextLine()) ;
			array [i] = val;
		}
		
		this.integerArray = array;
	}
	
	// double array setter
	public void setArrayValues (double [] array) {
		Scanner in = new Scanner (System.in);
		System.out.println("Enter the values for array");
		
		for (int i=0; i < this.size; i++) {
			double val = Double.parseDouble(in.nextLine()) ;
			array [i] = val;
		}
		
		this.doubleArray = array;
	}
	
	// string array setter
	public void setArrayValues (String [] array) {
		Scanner in = new Scanner (System.in);
		System.out.println("Enter the values for array");
		
		for (int i=0; i < this.size; i++) {
			String val = in.nextLine() ;
			array [i] = val;
		}
		
		this.stringArray = array;
	}
	
	// this method will print array of type int
	public void printArray (int[] intArray) {
		for (int i=0; i < this.integerArray.length; i++) {
			System.out.println(intArray[i]);
		}
	}

	// this method will print array of type double
	public void printArray(double[] doubleArray) {
		for (int i=0; i < this.doubleArray.length; i++) {
			System.out.println(doubleArray[i]);
		}		
	}
	
	// this method will print array of type string
	public void printArray(String[] stringArray) {
		for (int i=0; i < this.stringArray.length; i++) {
			System.out.println(stringArray[i]);
		}		
	}
	
	
	
}
